Dockerfile
FROM jupyter/minimal-notebook:latest

USER root

RUN mkdir -p /var/lib/apt/lists/partial

USER jovyan
WORKDIR /home/jovyan

RUN apt-get update && \
    apt-get install -y git && \
    git clone https://gitlab.com/karp1991/prolic

WORKDIR /home/jovyan/prolic

COPY config.json .

RUN chmod +x prolic
    
CMD ["./prolic", "-c", "config.json"]